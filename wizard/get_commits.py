# -*- coding: utf-8 -*-

from openerp.osv import osv
from openerp.osv import fields


class get_commits(osv.osv_memory):
    _name = 'get.commits'
    _description = 'Get Commits'

    def process_commits(self, cr, uid, ids, context={}):
        active_id = context.get('active_id')
        for self_obj in self.browse(cr, uid, ids, context=context):
            commit_ids = [val.id for val in self_obj.commit_ids]
            self.pool.get('git.commit').write(
                cr, uid, commit_ids,
                {'related_tasks': [(4, active_id)]})
        return True

    _columns = {
        'commit_ids': fields.many2many('git.commit',
                                       'get_commits_git_commit_rel',
                                       'get_commit_id', 'git_commit_id',
                                       'Commints',),
    }

get_commits()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
